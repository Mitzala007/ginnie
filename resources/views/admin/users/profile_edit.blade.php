@extends('admin.layouts.app')
@section('content')

    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url(config('siteVars.adm_pnl').'/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @include ('admin.error')
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">EDIT USERS </h3>
                        </div>

                        <div class="pad margin no-print">
                            <div style="margin-bottom: 0!important;" class="callout callout-info">
                                <h4><i class="fa fa-info"></i> Note:</h4>
                               Leave <strong>Password</strong> and <strong>Confirm Password</strong> empty if you are not going to change the password.
                            </div>
                        </div>

                        {!! Form::model($user, ['url' => url(config('siteVars.adm_pnl').'/profile_update/' . $user->id), 'method' => 'patch', 'files'=>true, 'class' => 'form-horizontal']) !!}

                            <div class="box-body">
                                @include ('admin.users.profile_form')
                            </div>

                            <div class="box-footer">
                                <button class="btn btn-info pull-right" type="submit">Edit</button>
                            </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
