@extends('admin.layouts.app')
@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                <h1>
                    {{ $menu }}
                    <small>Edit</small>
                </h1>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#">{{ $menu }}</a></li>
                <li class="active">edit</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit {{ $menu }}</h3>
                        </div>
                        {!! Form::model($static_page,['url' => url(config('siteVars.adm_pnl').'/static_page/'.$static_page->id),'method'=>'patch' ,'class' => 'form-horizontal','files'=>true]) !!}
                        <div class="box-body">
                            @include ('admin.static_page.form')
                        </div>
                        <div class="box-footer">
                            @if($static_page['id'] == 1)<a href="{{ url(config('siteVars.adm_pnl').'/about_us') }}" target="_blank"><input type="button" class="btn btn-danger" value="Preview"></a>@endif
                            @if($static_page['id'] == 2)<a href="{{ url(config('siteVars.adm_pnl').'/list_your_business') }}" target="_blank"><input type="button" class="btn btn-danger" value="Preview"></a>@endif
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection


