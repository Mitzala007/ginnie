<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <div class="col-sm-12">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description','id'=>'summernote']) !!}
        @if ($errors->has('description'))
            <span class="text-danger">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>