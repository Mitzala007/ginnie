<table id="example" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>User</th>
                            <th>Subject</th>
                            <th>Description</th>
                            <th>Reply</th>
                        </tr>
                        </thead>
                        <tbody id="sortable">
                        <?php $count = 0;?>
                        @foreach ($feedback as $list)
                            <?php $count++; ?>
                            <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $list['id'] }}" />
                            <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
                                <td>{{ $list['id'] }}</td>
                                <td>{{$list->user['name']}}</td>
                                <td>{{$list['title']}}</td>
                                <td>{{$list['description']}}</td>
                                @if($list['reply']!="")
                                    <td>{{$list['reply']}}</td>
                                @else
                                    <td>
                                        <div class="btn-group-horizontal">
                                            <a href="{{ url(config('siteVars.adm_pnl').'/feedback/'.$list['id'].'/edit')
                                             }}"
                                               class="btn btn-primary"><i class="fa fa-reply"></i></a>
                                        </div>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                    <div style="text-align:right;float:right; padding-right:0.4%; padding-top:0.5%;"> @include('admin.pagination.limit_links', ['paginator' => $feedback])</div>
                </div>
<script>
 
 $(function() {
    $('.selects').change(function() {
        var status = this.value; 
        var id = $(this).attr('sid');
        $.ajax({
            type: "GET",
            dataType: "json",   
            url: '{{url(config('siteVars.adm_pnl').'/status')}}',
            data: {'status': status, 'id': id},
            success: function(data){
              console.log(data.success);              
            }
        });
    });
    $('#searchbtn').on('click', function (e) {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        e.preventDefault();
        var service = $('#filter_service').val();
        var status = $('#filter_status').val();
        $.ajax({
            type: "GET",   
            url: '{{url(config('siteVars.adm_pnl').'/feedback')}}',
            data: {'status': status, 'service': service},
            success: function(data){
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });                  
            }
        });
    })
    $('#searchbtn').on('click', function (e) {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        e.preventDefault();
        var service = $('#filter_service').val();
        var status = $('#filter_status').val();
        $.ajax({
            type: "GET",   
            url: '{{url(config('siteVars.adm_pnl').'/feedback')}}',
            data: {'status': status, 'service': service},
            success: function(data){
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });                  
            }
        });
    })
  })
</script>