@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Show</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url(config('siteVars.adm_pnl').'/feedback')}}"><i class="fa fa-dashboard"></i> {{ $menu
                }}</a></li>
                <li class="active">Show</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Show {{$menu}}</h3>
                        </div>                        
                        <div class="box-body">
                            <table class="table table-bordered">
                                            
                                <tr>
                                    <th>User Name</th>
                                    <td>@foreach($users as $user)
                                            {{$user->name}}
                                        @endforeach
                                    </td> 
                                </tr>
                                <tr> 
                                    <th>Service</th>
                                    <td>@foreach($services as $service)
                                            {{$service->name}}
                                        @endforeach
                                    </td> 
                                <tr> 
                                <tr> 
                                    <th>Description</th>
                                    <td>{{ $request_management->description }}</td> 
                                <tr>  
                                <tr> 
                                    <th>status</th>
                                    <td>
                                        <div class="form-group">
                                            <select class="form-control selects" sid="{{ $request_management->id }}">
                                            @foreach (\App\UserRequest::$status as $key => $value)
                                                <option value="{{$key}}" {{ $key == $request_management->status ? 'selected' : '' }}>{{$value}}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </td> 
                                </tr>                         
                            </table>
                        </div>
                        <div class="box-footer">
                            <a href="{{ url(config('siteVars.adm_pnl').'/request_management') }}" ><button class="btn btn-default" type="button">Back</button></a>
                           
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script>
 
 $(function() {
    $('.selects').change(function() {
        var status = this.value; 
        var id = $(this).attr('sid');
        $.ajax({
            type: "GET",
            dataType: "json",   
            url: '{{url(config('siteVars.adm_pnl').'/status')}}',
            data: {'status': status, 'id': id},
            success: function(data){
              console.log(data.success);
              //location.reload();
              
            }
        });
    })
  })
</script>

