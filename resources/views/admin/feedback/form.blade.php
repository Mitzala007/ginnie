{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('reply') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="reply">Subject</label>
    <div class="col-sm-5">
        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => '','disabled']) !!}
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="description">Description</label>
    <div class="col-sm-5">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => '','disabled']) !!}
    </div>
</div>


<div class="form-group{{ $errors->has('reply') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="reply">Reply</label>
    <div class="col-sm-5">
        {!! Form::textarea('reply', null, ['class' => 'form-control', 'placeholder' => 'Reply']) !!}
        @if ($errors->has('reply'))
            <span class="help-block">
                <strong>{{ $errors->first('reply') }}</strong>
            </span>
        @endif
    </div>
</div>

