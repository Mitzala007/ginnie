@extends('admin.layouts.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url(config('siteVars.adm_pnl').'/request_management ') }}"><i class="fa fa-dashboard"></i> {{$menu}}</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>

            <div class="box box-info">
                <div class="box-header">
                <div class="col-md-2 col-sm-7">
                    <select class="form-control" id="filter_service">
                        <option value="">All service</option>
                         @foreach ($service as $list)
                        <option value="{{$list['id']}}">{{$list['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2 col-sm-7">
                    <div class="form-group">
                        <select class="form-control" id="filter_status">
                            <option value="">All status</option>
                                @foreach (\App\UserRequest::$status as $key => $value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                        </select>
                    </div>
               </div>
               <span style="float:left;">
                                <input type="submit" class="btn btn-info pull-right" id="searchbtn" value="Search">
                    </span>
                    <h3 class="box-title" style="float:right;">
                        <a href="{{ url(config('siteVars.adm_pnl').'/request_management') }}" ><button class="btn btn-default" type="button"><span class="fa fa-refresh"></span></button></a>
                    </h3>
                </div>
                <!-- /.box-header -->
                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">
                @include ('admin.request_management.table')
                </div>
        </section>
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }
</script>
  