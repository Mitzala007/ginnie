<table id="example" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <!-- <th>Edit</th> -->
                            <th>Id</th>
                            <th>User</th>
                            <th>Service</th>
                            <!-- <th>Seller</th> -->
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="sortable">
                        <?php $count = 0;?>
                        @foreach ($request_management as $list)
                            <?php $count++; ?>
                            <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $list['id'] }}" />
                            <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
                                <td>{{ $list['id'] }}</td>
                                <td>{{$list->user['name']}}</td> 
                                <td>{{$list->service['name']}}</td> 
                                <!-- <td>{{$list->seller['name']}}</td>                                                              -->
                                <td>
                                    <div class="form-group">
                                        <select class="form-control selects" sid="{{ $list['id'] }}">
                                        @foreach (\App\UserRequest::$status as $key => $value)
                                            <option value="{{$key}}" {{ $key == $list->status ? 'selected' : '' }}>{{$value}}</option>
                                        @endforeach
                                        </select>
                                     </div>
                                </td>

                                <td>
                                    <div class="btn-group-horizontal">
                                        <!-- <span data-toggle="tooltip" title="Delete Service" data-trigger="hover">
                                            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                                        </span> -->
                                         <a href="{{ url(config('siteVars.adm_pnl').'/request_management/'.$list['id']) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    </div>

                                </td>
                            </tr>

                         
                        @endforeach
                    </table>
                    <div style="text-align:right;float:right; padding-right:0.4%; padding-top:0.5%;"> @include('admin.pagination.limit_links', ['paginator' => $request_management])</div>
                </div>
<script>
 
 $(function() {
    $('.selects').change(function() {
        var status = this.value; 
        var id = $(this).attr('sid');
        $.ajax({
            type: "GET",
            dataType: "json",   
            url: '{{url(config('siteVars.adm_pnl').'/status')}}',
            data: {'status': status, 'id': id},
            success: function(data){
              console.log(data.success);              
            }
        });
    });
    $('#searchbtn').on('click', function (e) {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        e.preventDefault();
        var service = $('#filter_service').val();
        var status = $('#filter_status').val();
        $.ajax({
            type: "GET",   
            url: '{{url(config('siteVars.adm_pnl').'/request_management')}}',
            data: {'status': status, 'service': service},
            success: function(data){
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });                  
            }
        });
    })
    $('#searchbtn').on('click', function (e) {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        e.preventDefault();
        var service = $('#filter_service').val();
        var status = $('#filter_status').val();
        $.ajax({
            type: "GET",   
            url: '{{url(config('siteVars.adm_pnl').'/request_management')}}',
            data: {'status': status, 'service': service},
            success: function(data){
                $("#itemlist").empty().html(data);
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });                  
            }
        });
    })
  })
</script>