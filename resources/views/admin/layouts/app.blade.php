<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{config('siteVars.title')}} | {{ $menu }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <!-- Bootstrap 3.3.6 -->
    <link rel="icon" sizes="72x72" href="{{url('assets/website/img/favicon.png')}}">
    <link rel="stylesheet" href="{{url('assets/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{url('assets/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{url('assets/plugins/datepicker/datepicker3.css')}}">
    <!-- Date TIME Picker -->
    <link rel="stylesheet" href="{{url('assets/plugins/datetimepicker/bootstrap-datetimepicker.css')}}">
    <!-- Icheck radio -->
    <link rel="stylesheet" href="{{url('assets/plugins/iCheck/all.css')}}">
    <!-- SELECT  -->
    <link rel="stylesheet" href="{{url('assets/plugins/select2/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('assets/dist/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{url('assets/dist/css/skins/_all-skins.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{url('assets/plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{url('assets/plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{url('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}">

    <style type="text/css">
        .select2-container .select2-selection--single {
            height: 34px !important;
        }
    </style>
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
    <!-- Bootstrap datatable -->
    <link rel="stylesheet" href="{{url('assets/plugins/datatables/dataTables.bootstrap.css')}}">

    <link rel="stylesheet" href="{{url('assets/plugins/summernote/summernote.css')}}">


    <!-- jQuery 2.2.0 -->
    <script src="{{url('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>



    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" id="bodyid">
<div class="wrapper">

    <header class="main-header">
        <a href="{{ url(config('siteVars.adm_pnl').'/dashboard') }}" class="logo">
            <span class="logo-mini"><b>Gni</b></span>
            <span class="logo-lg"><b>{{config('siteVars.title')}}</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ URL::asset('assets/dist/img/avatar.png') }}" class="user-image"
                                 alt="User Image">
                            <span class="hidden-xs">{{ $user = Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="{{ URL::asset('assets/dist/img/avatar.png') }}" class="img-circle"
                                     alt="User Image">
                                <p>
                                    {{ $user = Auth::user()->name }} <br>
                                    <small></small>
                                </p>

                                <div class="pull-left" style="margin-top: -8px;">
                                    <a href="{{ url(config('siteVars.adm_pnl').'/profile_update/'.$user = Auth::user()->id).'/edit' }}"
                                       class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right" style="margin-top: -8px;">
                                    <a href="{{ url(config('siteVars.adm_pnl').'/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">

                <li class="@if($menu=='Dashboard') active  @endif treeview">
                    <a href="{{ url(config('siteVars.adm_pnl').'/dashboard') }}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <li class="@if($menu=='AppUser') active  @endif treeview">
                    <a href="{{ url(config('siteVars.adm_pnl').'/app_user') }}">
                        <i class="fa fa-th"></i> <span>User Management  </span>
                    </a>
                </li>

                <li class="@if($menu=='Category') active  @endif treeview">
                    <a href="{{ url(config('siteVars.adm_pnl').'/category') }}">
                        <i class="fa fa-th"></i> <span>Category</span>
                    </a>
                </li>

                <li class="@if($menu=='Service') active  @endif treeview">
                    <a href="{{ url(config('siteVars.adm_pnl').'/service') }}">
                        <i class="fa fa-th"></i> <span>Service</span>
                    </a>
                </li>
                <li class="@if($menu=='Request Management') active  @endif treeview">
                    <a href="{{ url(config('siteVars.adm_pnl').'/request_management') }}">
                        <i class="fa fa-th"></i> <span>Request Management</span>
                    </a>
                </li>

                <li class="@if($menu=='Feedback Management') active  @endif treeview">
                    <a href="{{ url(config('siteVars.adm_pnl').'/feedback') }}">
                        <i class="fa fa-th"></i> <span>Feedback Management</span>
                    </a>
                </li>

                <li class="treeview @if($menu == 'About Us' || $menu == 'List Your Business') active  @endif treeview">
                    <a href="#">
                        <i class="fa fa-th"></i><span> Static Page</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="@if($menu=='About Us') active  @endif treeview">
                            <a href="{{ url(config('siteVars.adm_pnl').'/static_page/1/edit') }}">
                                <i class="fa fa-circle-o"></i> <span>About Us</span>
                            </a>
                        </li>

                        <li class="@if($menu=='List Your Business') active  @endif treeview">
                            <a href="{{ url(config('siteVars.adm_pnl').'/static_page/2/edit') }}">
                                <i class="fa fa-circle-o"></i> <span>List Your Business</span>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    @yield('content')
    <footer class="main-footer">
        <strong>{{config('siteVars.title')}} Admin</strong>
    </footer>
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


{{--<!-- jQuery 2.2.0 -->--}}
{{--<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>--}}
<!-- jQuery UI 1.11.4 -->
<script src="{{ URL::asset('assets/plugins/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<script type="text/javascript">
    $('.calltype').click(function () {
        alert(this.val());
    });
</script>

<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap datatables -->
<script src="{{url('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- Select2 -->
<script src="{{url('assets/plugins/select2/select2.full.min.js')}}"></script>

{{--@yield('jquery')--}}
        <!-- Bootstrap 3.3.6 -->
<script src="{{url('assets/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{url('assets/plugins/iCheck/icheck.min.js')}}"></script>
<!-- Morris.js charts -->

<!-- InputMask -->
<script src="{{url('assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{url('assets/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{url('assets/plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{url('assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{url('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ URL::asset('assets/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="{{url('assets/plugins/moment.min.js')}}"></script>
<script src="{{url('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('assets/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- bootstrap time picker -->
<script src="{{url('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<!-- bootstrap DATE time picker -->
<script src="{{url('assets/plugins/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/plugins/datetimepicker/locales/bootstrap-datetimepicker.fr.js')}}" charset="UTF-8"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{url('assets/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('assets/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('assets/dist/js/app.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{--<script src="{{ URL::asset('assets/dist/js/pages/dashboard.js')}}"></script>--}}

<script src="{{url('assets/dist/js/demo.js')}}"></script>

<script src="{{ URL('assets/dist/js/custom.js')}}"></script>

<script src="{{url('assets/plugins/summernote/summernote.js')}}"></script>
{{--<script src="{{ url('js/summernote-file.js')}}"></script>--}}
<script type="text/javascript" src="{{url('assets/plugins/summernote-map/summernote-map-plugin.js')}}"></script>

{{--<script src="{{ URL::asset('assets/plugins/ckeditor/ckeditor.js')}}"></script>--}}
{{--<script src="{{ URL::asset('assets/plugins/ckeditor/ckfinder/ckfinder.js')}}"></script>--}}


<script>
    $(function () {

        $(".select2").select2();
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
        $('#example').DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": false
        });
        
        $('input:radio[id=admin]').attr('checked',true);

        $('.datepicker').datepicker({
            format: 'yyyy-m-d',
            autoclose: true
        });
    });
</script>


<script type="text/javascript">

    $(function (){

        $("textarea[name=description132]").summernote({
            height: 280,
            map: {
                apiKey: 'AIzaSyBxRUcdPvPl8JIO1ay1LeB_HdFQnSHccwc',
                // This will be used when map is initialized in the dialog.
                center: {
                    lat: 22.2981842,
                    lng: 114.172221
                },
                zoom: 13
            },
            toolbar: [
                // [groupName, [list of button]]

                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize', 'height']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['table','picture','video','link','map','minidiag']],
                ['misc', ['fullscreen', 'codeview']],
            ],
            callbacks: {
                onImageUpload: function(files) {
                    for (var i = 0; i < files.length; i++)
                        upload_image(files[i], this);
                }
            },
        });
    });
</script>

<script type="text/javascript">



    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });

    $('#summernote').summernote({
        height: 250,
        toolbar: [
            // [groupName, [list of button]]

            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize','font', 'height']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['table','picture','video','link','map','minidiag']],
            ['misc', ['fullscreen', 'codeview']],
        ],
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                for(var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });

    $('#summernote1').summernote({
        height: 250,
        toolbar: [
            // [groupName, [list of button]]

            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize','font', 'height']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['insert', ['table','picture','video','link','map','minidiag']],
            ['misc', ['fullscreen', 'codeview']],
        ],
        callbacks: {
            onImageUpload: function(files, editor, welEditable) {
                for(var i = files.length - 1; i >= 0; i--) {
                    sendFile(files[i], this);
                }
            }
        }
    });

    function sendFile(file, el) {
        var form_data = new FormData();
        form_data.append('image', file);
        $.ajax({
            data: form_data,
            type: "POST",
            url: '{{url(config('siteVars.adm_pnl').'/image/upload')}}',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                $(el).summernote('editor.insertImage', url);
            }
        });
    }
</script>
</body>
</html>
