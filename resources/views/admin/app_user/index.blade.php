@extends('admin.layouts.app')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{$menu}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url(config('siteVars.adm_pnl').'/app_user') }}"><i class="fa fa-dashboard"></i> {{$menu}}</a></li>
            </ol>
        </section>
        <section class="content">
            @include ('admin.error')
            <div id="responce" name="responce" class="alert alert-success" style="display: none">
            </div>

            <div class="box box-info">
                <div class="box-header"> 
                    <div class="col-md-5 col-sm-7">
                            <span style="float:left; padding-top: 5px" class="col-md-5 col-sm-8 col-xs-12">
                                <input  class="form-control" type="text" @if(!empty($search)) value="{{$search}}" @else placeholder="Search" @endif name="search" id="search">
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                            <button id="search_data" class="btn btn-info pull-right">search</button>
                            </span>
                            <span style="float:left; padding-top: 5px" class="col-md-5 col-sm-9 col-xs-12">
                                <select class="form-control selects">
                                    <option value="">Please Select </option>
                                    <option value="1">User</option>
                                    <option value="2">Seller</option>                                
                                </select>
                            </span>
                    </div>
                    <h3 class="box-title" style="float:right;">
                        <a href="{{ url(config('siteVars.adm_pnl').'/app_user/create') }}" ><button class="btn btn-info" type="button"><span class="fa fa-plus"></span></button></a>
                        <a href="{{ url(config('siteVars.adm_pnl').'/app_user') }}" ><button class="btn btn-primary" type="button"><span class="fa fa-refresh"></span></button></a>
                    </h3>
                </div>
                <!-- /.box-header -->
                @include('admin.loader')
                <div class="box-body table-responsive" id="itemlist">
                    @include ('admin.app_user.table')
                </div>
            </div>
        </section>
    </div>
@endsection

<script src="{{ URL::asset('assets/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/ladda/ladda-themeless.min.css')}}">
<script src="{{ URL::asset('assets/plugins/ladda/spin.min.js')}}"></script>
<script src="{{ URL::asset('assets/plugins/ladda/ladda.min.js')}}"></script>
<script>Ladda.bind( 'input[type=submit]' );</script>
<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        var page = $(this).attr('href');
        window.history.pushState("", "", page);
        getPagination(page);

    })

    function getPagination(page)
    {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";

        $.ajax({
            url : page
        }).done(function (data) {
            $("#itemlist").empty().html(data);
            document.getElementById('bodyid').style.opacity=1;
            document.getElementById('load').style.display="none";
            $('#example').DataTable({
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": true
            });
        }).fail(function () {
            alert('Pagination could not be loaded.');
        });
    }
</script>
  