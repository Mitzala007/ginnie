@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url(config('siteVars.adm_pnl').'/app_user')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit {{$menu}}</h3>
                        </div>
                        {!! Form::model($app_user,['url' => url(config('siteVars.adm_pnl').'/app_user/'.$app_user->id),'method'=>'patch' ,'class' => 'form-horizontal','files'=>true]) !!}
                        <div class="box-body">
                            @include ('admin.app_user.form')
                        </div>
                        <div class="box-footer">
                            <a href="{{ url(config('siteVars.adm_pnl').'/app_user') }}" ><button class="btn btn-default" type="button">Back</button></a>
                            <button class="btn btn-info pull-right" type="submit">Edit</button>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection




