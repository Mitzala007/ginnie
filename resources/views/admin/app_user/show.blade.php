@extends('admin.layouts.app')

@section('content')
    <div class="content-wrapper" style="min-height: 946px;">
        <section class="content-header">
            <h1>
                {{ $menu }}
                <small>Show</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{url(config('siteVars.adm_pnl').'/app_user')}}"><i class="fa fa-dashboard"></i> {{ $menu }}</a></li>
                <li class="active">Show</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Show {{$menu}}</h3>
                        </div>                        
                        <div class="box-body">
                            <table class="table table-bordered">                
                                <tr>
                                    <th>Name</th>
                                    <td>{{$appuser->name}}</td> 
                                </tr>
                                <tr> 
                                    <th>Email</th>
                                    <td>{{ $appuser->email }}</td> 
                                <tr> 
                                <tr> 
                                    <th>Address</th>
                                    <td>{{ $appuser->address }}</td> 
                                <tr> 
                                <tr> 
                                    <th>Phone</th>
                                    <td>{{ $appuser->phone }}</td> 
                                <tr> 
                                <tr> 
                                    <th>Birthdate</th>
                                    <td>{{ $appuser->birthdate }}</td> 
                                <tr> 
                                <tr> 
                                    <th>image</th>
                                    <td>
                                        @if($appuser->image!="" && file_exists($appuser->image))
                                            <img src="{{ url($appuser->image) }}" width="50">
                                        @endif
                                    </td> 
                                <tr> 
                                <tr> 
                                    <th>status</th>
                                    <td>
                                        @if($appuser->status == 'active')
                                            <div class="btn-group-horizontal">
                                                 <button class="btn btn-success"  type="button" style="height:28px; padding:0 12px">Active</button>
                                            </div>
                                        @else
                                            <div class="btn-group-horizontal">
                                                <button class="btn btn-danger"  type="button" style="height:28px; padding:0 12px">In Active</button>
                                            </div>
                                        @endif
                                    </td> 
                                <tr>                         
                            </table>
                        </div>
                        <div class="box-footer">
                            <a href="{{ url(config('siteVars.adm_pnl').'/app_user') }}" ><button class="btn btn-default" type="button">Back</button></a>
                           
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection



