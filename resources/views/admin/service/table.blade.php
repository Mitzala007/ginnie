<table id="example" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Edit</th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="sortable">
                        <?php $count = 0;?>
                        @foreach ($service as $list)
                            <?php $count++; ?>
                            <input type="hidden" name="pid<?php echo $count;?>" id="pid<?php echo $count;?>" value="{{ $list['id'] }}" />
                            <tr class="ui-state-default" id="arrayorder_{{$list['id']}}">
                                <td>
                                    <div class="btn-group-horizontal">
                                        {{ Form::open(array('url' => config('siteVars.adm_pnl').'/service/'.$list['id'].'/edit', 'method' => 'get','style'=>'display:inline')) }}
                                        <button class="btn btn-info tip" data-toggle="tooltip" title="Edit service" data-trigger="hover" type="submit" ><i class="fa fa-edit"></i></button>
                                        {{ Form::close() }}
                                    </div>
                                </td>
                                <td>{{ $list['id'] }}</td>
                                <td>{{ $list['name'] }}</td>                                
                                <td>{{$list->category['name']}}</td>                               
                                <td>
                                    @if($list['status'] == 'active')
                                        <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" >
                                            <button class="btn btn-success unassign ladda-button" data-style="slide-left" id="remove" ruid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span> </button>
                                        </div>
                                        <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"  style="display: none"  >
                                            <button class="btn btn-danger assign ladda-button" data-style="slide-left" id="assign" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                                        </div>
                                    @endif
                                    @if($list['status'] == 'inactive')
                                        <div class="btn-group-horizontal" id="assign_add_{{ $list['id'] }}"   >
                                            <button class="btn btn-danger assign ladda-button" id="assign" data-style="slide-left" uid="{{ $list['id'] }}"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">In Active</span></button>
                                        </div>
                                        <div class="btn-group-horizontal" id="assign_remove_{{ $list['id'] }}" style="display: none" >
                                            <button class="btn  btn-success unassign ladda-button" id="remove" ruid="{{ $list['id'] }}" data-style="slide-left"  type="button" style="height:28px; padding:0 12px"><span class="ladda-label">Active</span></button>
                                        </div>
                                    @endif
                                </td>

                                <td>
                                    <div class="btn-group-horizontal">
                                        <span data-toggle="tooltip" title="Delete Service" data-trigger="hover">
                                            <button class="btn btn-danger" type="button" data-toggle="modal" data-target="#myModal{{$list['id']}}"><i class="fa fa-trash"></i></button>
                                        </span>
                                    </div>

                                </td>
                            </tr>

                            <div id="myModal{{$list['id']}}" class="fade modal modal-danger" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Delete {{$menu}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure you want to delete this {{$menu}} ?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-outline" onclick="destroy_service({{$list['id']}})">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </table>
                    <div style="text-align:right;float:right; padding-right:0.4%; padding-top:0.5%;"> @include('admin.pagination.limit_links', ['paginator' => $service])</div>
                    <script type="text/javascript">
    $(document).ready(function(){
        $('.assign').click(function(){
            var user_id = $(this).attr('uid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url(config('siteVars.adm_pnl').'/service/assign')}}',
                type: "post",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).show();
                    $('#assign_add_'+user_id).hide();
                }
            });
        });

        $('.unassign').click(function(){
            var user_id = $(this).attr('ruid');
            var l = Ladda.create(this);
            l.start();
            $.ajax({
                url: '{{url(config('siteVars.adm_pnl').'/service/unassign')}}',
                type: "post",
                data: {'id': user_id,'_token' : $('meta[name=_token]').attr('content')},
                success: function(data){
                    l.stop();
                    $('#assign_remove_'+user_id).hide();
                    $('#assign_add_'+user_id).show();
                }
            });
        });
    });
    $('#search_data').on('click', function (e) {
        document.getElementById('bodyid').style.backgroundColor= 'rgba(' + 0 + ',' + 0 + ',' + 0 + 0.3 +' )';
        document.getElementById('bodyid').style.opacity=0.5;
        document.getElementById('load').style.display="block";
        e.preventDefault();
        $value = $("input#search").val();
        $.ajax({
            type:"GET",                    
            url: '{{url(config('siteVars.adm_pnl').'/service')}}',
            data: {'search': $value},
            success: function (data) {
                //console.log(data);
                $("#itemlist").empty().html(data);  
                document.getElementById('bodyid').style.opacity=1;
                document.getElementById('load').style.display="none";
                $('#example').DataTable({
                    "paging": false,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": true
                });                              
            }
        })
    })
    function destroy_service(id)
    {
        $.ajax({
            url:'service/'+id,
            type:'delete',
            data:{'id':id},
            success:function(data)
            {   location.reload()
                var new_url = 'service';
                window.location.href = new_url;
            }
        });
    }

</script>