{!! Form::hidden('redirects_to', URL::previous()) !!}

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="title">Name <span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="description">Description</label>
    <div class="col-sm-5">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="category_id">Category<span class="text-red">*</span></label>
    <div class="col-sm-5">
        {!! Form::select('category_id', $category, !empty($category_selected)?$category_selected:null, ['class' => 'select2 form-control', 'style' => 'width: 100%']) !!}
        @if ($errors->has('category_id'))
            <span class="help-block">
                <strong>{{ $errors->first('category_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
    <label class="col-sm-2 control-label" for="role" id="status_lb">Status <span class="text-red">*</span></label>
    <div class="col-md-5 col-sm-6">
        @foreach (\App\Service::$status as $key => $value)
            <label>
                @if($key == 'active')
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key, 'checked']) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @else
                    {!! Form::radio('status', $key, null, ['class' => 'flat-red','id'=>'status_'.$key]) !!} <span style="margin-right: 10px" id="status_val_{{$key}}">{{ $value }}</span>
                @endif
            </label>
        @endforeach
        @if ($errors->has('status'))
            <span class="help-block">
                 <strong>{{ $errors->first('status') }}</strong>
                </span>
        @endif
        <strong><span id="status_msg" style="color: #dd4b39; margin: 5px 0 10px 0;"></span></strong>
    </div>
</div>