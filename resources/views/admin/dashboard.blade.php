@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active"><a href="{{url(config('siteVars.adm_pnl').'/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-info box-body">
        <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                    <h3>{{$appuser}}</h3>
                    <p>App User</p>
                    </div>
                    <div class="icon">                   
                    <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{ url(config('siteVars.adm_pnl').'/app_user ') }}" class="small-box-footer">View All<i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                    <h3>{{$category}}</h3>
                    <p>Category</p>
                    </div>
                    <div class="icon">
                    <i class="fa fa-sitemap"></i>
                    </div>
                    <a href="{{ url(config('siteVars.adm_pnl').'/category ') }}" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                    <h3>{{$service}}</h3>
                    <p>Service</p>
                    </div>
                    <div class="icon">
                    <i class="ion ion-bag"></i>            
                    </div>
                    <a href="{{ url(config('siteVars.adm_pnl').'/service ') }}" class="small-box-footer">View All<i class="fa fa-arrow-circle-right"></i></a>
                </div>
                </div>
                <!-- ./col -->
            </div>
        <!-- Main content -->
        </div>
    </section>
</div>
@endsection






