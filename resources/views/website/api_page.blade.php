<!DOCTYPE html>
<html lang="en">
<head>
    <title>The One | {{$menu}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ URL::asset('assets/dist/css/AdminLTE.min.css')}}">
</head>
<body>
<div class="bg-img1 size1">
{{--    <div class="flex-w flex-sa respon1">--}}
{{--        <div class="m-t-15">--}}
{{--            <h2 align="center" class="l1-txt2">--}}
{{--                {{$menu}}--}}
{{--            </h2>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="flex-w flex-sa respon1">
        <div class="p-l-250 p-r-250 p-t-18 p-b-18 p-lr-15-sm">
            {!! $page['description'] !!}
        </div>
    </div>
</div>
</body>
</html>
