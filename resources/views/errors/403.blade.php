<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page not found.</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.min.css') }}">

    <style type="text/css">
        .center {text-align: center; margin-left: auto; margin-right: auto; margin-bottom: auto; margin-top: auto;}
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="container" style="margin-top: 10%">
    <div class="row">
        <div class="span12">
            <div class="hero-unit center">
                <h1 style="color: #181920;line-height: 1.5;font-family: Bellefair,serif;"><small><font face="Tahoma" color="red">403 Forbidden</font></small></h1>
                <br />
                <p style="color: #8b8e94; font-family: Open Sans,Arial,sans-serif;">Access Denied You don't have permission to access.
            </div>
        </div>
    </div>
</div>
</body>
</html>