<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login','api\AppUserController@login');
Route::post('logout','api\AppUserController@logout');
Route::post('register','api\AppUserController@register');
Route::post('update_profile','api\AppUserController@update_profile');
Route::post('get_profile','api\AppUserController@get_profile');
Route::post('forgot_password','api\AppUserController@forgot_password');
Route::post('add_request','api\AppUserController@add_request');
Route::post('my_request','api\AppUserController@my_request');
Route::post('get_category','api\AppUserController@get_category');
Route::post('get_service','api\AppUserController@get_service');
Route::post('get_seller','api\AppUserController@get_seller');
Route::post('change_password','api\AppUserController@change_password');
Route::post('add_service','api\AppUserController@add_service');
Route::post('delete_service','api\AppUserController@delete_service');
Route::post('my_service','api\AppUserController@my_service');
Route::post('add_feedback','api\AppUserController@add_feedback');
Route::post('get_feedback','api\AppUserController@get_feedback');