<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::middleware('auth')->prefix('Admin','Admin/home')->group( function() {
Route::group(['prefix' => 'gni_ctlpanel','gni_ctlpanel/home'], function() {

    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::auth();
    Route::get('/','admin\DashboardController@index');
    Route::get('dashboard','admin\DashboardController@index');

    /* User Managment */
    Route::resource('profile_update', 'admin\ProfileupdateController');
    Route::post('app_user/assign','admin\AppUserController@assign');
    Route::post('app_user/unassign','admin\AppUserController@unassign');
    Route::resource('app_user', 'admin\AppUserController');

    /* category Management */    
    Route::resource('category', 'admin\CategoryController');
    Route::post('category/assign','admin\CategoryController@assign');
    Route::post('category/unassign','admin\CategoryController@unassign');

    /* service Management */ 
    Route::resource('service', 'admin\ServiceController');
    Route::post('service/assign','admin\ServiceController@assign');
    Route::post('service/unassign','admin\ServiceController@unassign');

    // request management
    Route::resource('request_management', 'admin\RequestManagementController');

    // feedback management
    Route::resource('feedback', 'admin\FeedbackController');

    Route::get('status', 'admin\RequestManagementController@changeStatus');

    /*Staticpage Managment*/
    Route::resource('static_page', 'admin\StaticPageController');
    
    Route::get('about_us', 'api\StaticPageController@about_us');
    Route::get('list_your_business', 'api\StaticPageController@list_your_business');

    Auth::routes();

});

