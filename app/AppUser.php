<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\RequestManagement;

class AppUser extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','email','password','address','service_id','phone','birthdate','image','thumbnail_image','rememberToken','latitude','longitude','status'
    ];

    protected $casts = ['phone' => 'integer' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','created_at','deleted_at','updated_at','status',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    
    protected $statusactive ='active';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    public function userrequest()
    {
        return $this->hasMany(UserRequest::class);
    }

    public function feedback()
    {
        return $this->hasMany(Feedback::class);
    }
}
