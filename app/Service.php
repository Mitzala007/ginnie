<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\RequestManagement;

class Service extends Model
{
    protected $fillable = [
        'name','description','category_id','seller_id','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function userrequest()
    {
        return $this->belongsTo(UserRequest::class);
    }
}
