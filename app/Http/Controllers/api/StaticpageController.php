<?php

namespace App\Http\Controllers\api;

use App\StaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{
    public function about_us()
    {
        $data['menu'] = 'About Us';
        $data['page']=StaticPage::findOrFail(1);
        return view('website.api_page',$data);
    }

    public function list_your_business()
    {
        $data['menu'] = 'list Your Business';
        $data['page']=StaticPage::findOrFail(2);
        return view('website.api_page',$data);
    }

    public function chaperone_information()
    {
        $data['menu'] = 'Chaperone Information';
        $data['page']=StaticPage::findOrFail(3);
        return view('website.api_page',$data);
    }
}
