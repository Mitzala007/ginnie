<?php

namespace App\Http\Controllers\api;

use App\AppUser;
use App\Feedback;
use App\UserRequest;
use App\Category;
use App\Seller;
use App\Service; 
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;

class AppUserController extends Controller
{
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'password' => 'required',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }
            $appuser = AppUser::where("email", $request['email'])->where('status','active')->first();

            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "Email address or password is incorrect. Please try again.";
                return response($response, 200);
            }
            else {

                if (Hash::check($request['password'], $appuser['password']) == false) {
                    $response["Result"] = 0;
                    $response["Message"] = "Email address or password is incorrect. Please try again.";
                    return response($response, 200);
                }
                else {

                    if (isset($request['device_type']) && isset($request['device_token'])){
                        $logins['device_type'] = $request['device_type'];
                        $logins['device_token'] = $request['device_token'];
                    }

                    $AppUser = AppUser::findOrFail($appuser->id);
                    $AppUser->rememberToken = str_random(30);
                    $AppUser->save();

                    $input=$request->all();
                    $AppUser->update($input);

                    $this->remove_null($AppUser);
                    $AppUser->image = file_exists($AppUser->image)? url($AppUser->image):"";
                    $AppUser->thumbnail_image = file_exists($AppUser->thumbnail_image)? url($AppUser->thumbnail_image):"";

                    $response["Result"] = 1;
                    $response["User"] = $AppUser;
                    $response['Message'] = "User login Successfully.";
                    return response($response, 200);
                }
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function logout(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser) || $request['session_token'] == "") {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 401);
            }

            if (!empty($loginUser)) {

                $appuser = AppUser::findorFail($loginUser->id);
                $appuser->rememberToken =NULL;
                $appuser->save();

                $response["Result"] = 1;
                $response['Message'] = "Success";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|email|unique:app_users,email',
                    'password' => 'required',
                    'phone' => 'required|numeric',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $input = $request->all();
            $input['rememberToken'] = str_random(30);
            $appuser = AppUser::create($input);

            /*Login Detail*/
            if (isset($request['device_type']) && isset($request['device_token'])){
                $logins['device_type'] = $request['device_type'];
                $logins['device_token'] = $request['device_token'];
            }

            $user = AppUser::findorFail($appuser->id);

            $this->remove_null($user);

            $response["Result"] = 1;
            $response["User"] = $user;
            $response['Message'] = "User Registered Successfully.";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function update_profile(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where('rememberToken', $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 401);
            }
            $appuser = AppUser::findorFail($loginUser->id);

                $input = $request->all();

            if ($photo = $request->file('image')) {
                $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();

                /*Image Resize Code*/
                $img_resize = Image::make($photo->getRealPath());
                $data = getimagesize($photo);
                $width = $data[0];

                $img_resize->save(public_path('resource/app_users/'.$img_name));
                /*-------------*/
                $input['image'] = "public/resource/app_users/".$img_name;

                /* THUMBNAIL*/

                $path1 =  $this->image($photo,'app_users/thumbnail');
                if($width > 200){
                    $input['thumbnail_image'] = $this->Imagethumbnail($path1,'app_users/thumbnail',500,500,null);
                }
                else{
                    $input['thumbnail_image'] = $path1;
                }
            }

            $appuser->update($input);
            $user = AppUser::findorFail($appuser->id);

            $user->image = file_exists($user->image)? url($user->image):"";
            $user->thumbnail_image = file_exists($user->thumbnail_image)? url($user->thumbnail_image):"";
            $this->remove_null($user);

            $response["Result"] = 1;
            $response["User"] = $user;
            $response['Message'] = "User Updated Successfully.";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_profile(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'session_token' => 'required',

            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

           $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }

            $appuser = AppUser::where('id',$loginUser->id)->get();
            foreach ($appuser as $data){
                $Service = Service::whereIn('id',explode(',',$data['service_id']))->get();
                foreach ($Service as $v_style){
                    $this->remove_null($v_style);
                }

                $data['Service'] = $Service;
                $this->remove_null($data);
            }
            $this->remove_null($appuser);
            $response["Result"] = 1;
            $response["User"] = $appuser;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

   
    public function forgot_password(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                ]
            );

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $appuser = AppUser::where("email", $request['email'])->first();

            if (empty($appuser)) {
                $response["Result"] = 0;
                $response["Message"] = "Invalid email address.";
                return response($response, 200);
            } else {
                $pass=str_random(8);
                $input['password']=$pass;
                $appuser->update($input);

                $to1=$request['email'];
                $from1='noreply@dob.com';
                $subject1='Forgot Password';
                $mailcontent1 = "<html>
                                <head>
                                    <meta charset=\"UTF-8\">
                                    <title>DOB</title>
                                    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
                                </head>
                                
                                <body style=\"color: #000000; font-size: 16px; font-weight: 400;\">
                                
                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"100%\">
                                    <tr>
                                        <td height=\"20\"></td>
                                    </tr>
                                    <tr>
                                        <td align=\"center\" valign=\"top\">
                                            <table cellpadding=\"0\" cellspacing=\"0\" width=\"700px\" style=\"border:thin;border-color:#6F1749; border-style:solid;\">
                                                <tr>
                                                    <td>
                                                        <div style=\"padding:5px 10px 5px 20px; background-color: #321464; color:#FFFFFF; font-size:16px;\">
                                                            <h2 style=\"color: #ffffff; text-align: center\">".'Forgot Password'."</h2>
                                                        </div>
                                                        <div style=\"padding:20px 10px 35px 30px;\">
                                                            Dear <b>".$appuser['name']."</b>,<br/><br/>
                                                            <i>Your new password is:</i>
                                                            <b>".$pass."</b>
                                                        </div>
                                                        <div style=\"padding:10px 10px 10px 30px; background-color: #321464; color:#FFFFFF; font-size:12px;\">
                                                            <p style=\"color: #FFFFFF; text-align: center\">&copy; Dance Off Bro</p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>                                
                                </body>
                                </html>";
                $headers  = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                $headers .= "From: $from1\r\n";
                mail($to1,$subject1,$mailcontent1,$headers);
                
                $response["Result"]=1;
                $response["Message"] = "A new password has been sent to your registered email address.";
                return response($response, 200);
            }
        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_request(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
                'service_id'=>'required|exists:services,id',
                'seller_id'=>'required|exists:sellers,id',
                'description'=> 'required'
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }
            
            $input = $request->all();
            $input['user_id'] = $loginUser->id;
            $request = UserRequest::create($input);

            $this->remove_null($request);

            $response["Result"] = 1;
            $response['Message'] = "Request inserted Successfully.";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }
    
    public function my_request(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }
           $UserRequest = UserRequest::where("user_id",$loginUser->id)->orderby('id', 'desc')->get();
           foreach ($UserRequest as $dst){
            $this->remove_null($dst);
            }
            $response["Result"] = 1;
            $response["My_Request"] = $UserRequest;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }
    public function get_category(Request $request)
    {
        try {
            
            $category= Category::where('status','active')->get();
            foreach ($category as $dst){
                $this->remove_null($dst);
            }
            $response["Result"]= 1;
            $response["category"] = $category;
            $response["Message"] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }
    public function get_service(Request $request)
    {
        try {   
            if($request['category_id']) {
                $service = Service::with(['category'])->where("category_id", $request['category_id'])->get();
            } else {       
                $service  = Service::with(['category'])->where('status','active')->get();
            }         
            foreach ($service as $dst){
                $this->remove_null($dst);
                $this->remove_null($dst->category);
            }
            $response["Result"]=1;
            $response["service"] = $service;
            $response["Message"] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    } 
    
    public function get_seller(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'latitude' => 'required',
                'longitude'=>'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }


            $query = AppUser::select("*",DB::raw("6371 * acos(cos(radians(" . $request->latitude. ")) 
                    * cos(radians(latitude)) 
                    * cos(radians(longitude) - radians(" . $request->longitude . ")) 
                    + sin(radians(" .$request->latitude. ")) 
                    * sin(radians(latitude))) AS distance"));

            if(isset($request['service_id']) && $request['service_id']!=""){
                $service_id = $request['service_id'];
                $query = $query->whereRaw("find_in_set($service_id, service_id)");
            }

            $seller = $query->having('distance','<=','50')
                ->whereNotNull('service_id')->get();
            
                foreach ($seller as $data){
                    $Service = Service::whereIn('id',explode(',',$data['service_id']))->get();
                    foreach ($Service as $v_style){
                        $this->remove_null($v_style);
                    }

                    $data['distance'] = number_format((float)$data['distance'], 2, '.', '');;
                    $data['Service'] = $Service;
                    $this->remove_null($data);
                }
            $response["Result"]=1;
            $response["seller"] = $seller;
            $response["Message"] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }
    
    public function change_password(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
                'old_password'=>'required',
                'new_password'=>'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }
            if (!(Hash::check($request->old_password, $loginUser->password))){
                $response["Result"] = 0;
                $response["Message"] = "Old Password didn't match";
                return response($response, 200);
            }
                $appuser = AppUser::findorFail($loginUser->id);
                $input['password']=$request->new_password;
                $appuser->update($input);
                
            $response["Result"] = 1;
            //$response["User"] = $appuser;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }
    public function add_service(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'session_token' => 'required',
                'category_id'=>'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }
            if(isset($request['id']) && $request['id']!="")
            {   
                $services = Service::where("id", $request['id'])->first();;
                if(!empty($services))
                {   $service = Service::findorFail($request['id']);
                    $input = $request->all();
                    $input['seller_id'] = $loginUser->id;
                    $service->update($input);

                    $appuser = AppUser::findorFail($loginUser->id);
                    $seller_id = Service::where('seller_id',$loginUser->id)->pluck('id')->implode(',');
                    $input['service_id']= $seller_id;
                    unset($input['name']);
                    $appuser->update($input);

                    $response["Result"] = 1;
                    $response['Message'] = "Service Updated Successfully.";
                    return response($response, 200);
                }
                else
                {                 
                    $response["Result"] = 0;
                    $response["Message"] = "Id not found";
                    return response($response, 200);
                }
                
            }
            else
            {
                $input = $request->all();
                $input['seller_id'] = $loginUser->id;
                $service = Service::create($input);

                $appuser = AppUser::findorFail($loginUser->id);
                $seller_id = Service::where('seller_id',$loginUser->id)->pluck('id')->implode(',');
                $input['service_id']= $seller_id;
                unset($input['name']);
                $appuser->update($input);

                $response["Result"] = 1;
                $response['Message'] = "Service inserted Successfully.";
                return response($response, 200);
            }

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function delete_service(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
                'service_id'=>'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }
            $Service = Service::where('seller_id',$loginUser->id)->where('id', $request['service_id'])->first();
            if(empty($Service))
            {
                $response["Result"] = 0;
                $response["Message"] = "invalid Service id.";
                return response($response, 200);

            }else{
                $service = Service::findOrfail($request->service_id);
                $Service->delete();
               
            $appuser = AppUser::findorFail($loginUser->id);
            $seller_id = Service::where('seller_id',$loginUser->id)->pluck('id')->implode(',');
            $input['service_id']= $seller_id;
            $appuser->update($input);
                $message = "Service deleted Successfully.";
            }        
            
            $response["Result"] = 1;
            $response['Message'] = $message;
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }
    public function my_service(Request $request)
    {
        try { 
            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }
            
            $service = Service::with(['category'])->where("seller_id",$loginUser->id)->get();        
            foreach ($service as $dst){
                $this->remove_null($dst);
                $this->remove_null($dst->category);
            }
            $response["Result"]=1;
            $response["service"] = $service;
            $response["Message"] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function add_feedback(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
                'title'=>'required',
                'description'=> 'required'
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }

            $input = $request->all();
            $input['user_id'] = $loginUser->id;
            $feedback = Feedback::create($input);

            $get_feedback = Feedback::where('id',$feedback['id'])->first();

            $this->remove_null($get_feedback);

            $response["Result"] = 1;
            $response["Feedback"] = $get_feedback;
            $response['Message'] = "Feedback submitted Successfully!";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }

    public function get_feedback(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'session_token' => 'required',
            ]);

            if ($validator->fails()) {
                $response["Result"] = 0;
                $response["Message"] = implode(',', $validator->errors()->all());
                return response($response, 200);
            }

            $loginUser = AppUser::where("rememberToken", $request['session_token'])->first();

            if (empty($loginUser)) {
                $response["Result"] = 0;
                $response["Message"] = "Your account may be logged in from other device or deactivated, please try to login again.";
                return response($response, 200);
            }
            $feedback = Feedback::where("user_id",$loginUser->id)->orderby('id', 'desc')->get();
            foreach ($feedback as $dst){
                $this->remove_null($dst);
            }
            $response["Result"] = 1;
            $response["My_Feedback"] = $feedback;
            $response['Message'] = "Success";
            return response($response, 200);

        } catch (Exception $e) {
            return response("", 500);
        }
    }
}