<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Intervention\Image\Facades\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $pagination=10;
    public function image($photo,$path)
    {
        /* IMAGE UPLOAD VALIDATION */
        $img_ext = $photo->getClientOriginalExtension();
        if ($img_ext=="jpeg" || $img_ext=="jpg" || $img_ext=="png" || $img_ext=="bmp" || $img_ext=="gif" || $img_ext=="JPEG" || $img_ext=="JPG" || $img_ext=="PNG" || $img_ext=="BMP" || $img_ext=="GIF" ) {}
        else{
            return back()->withInput()->withErrors(['image' => 'Invalid file type.']);
        }
        /* ----------------------- */

        $root = base_path() . '/public/resource/'.$path ;
        $name = str_random(20).".".$photo->getClientOriginalExtension();
        $mimetype = $photo->getMimeType();
        $explode = explode("/",$mimetype);
        $type = $explode[0];

        if (!file_exists($root)) {
            mkdir($root, 0777, true);
        }
        $photo->move($root,$name);
        return $path = "public/resource/".$path."/".$name;
    }

    public function file($photo,$path)
    {
        /* IMAGE UPLOAD VALIDATION */
        $img_ext = $photo->getClientOriginalExtension();

        /* ----------------------- */

        $root = base_path() . '/public/resource/'.$path ;
        $name = str_random(20).".".$photo->getClientOriginalExtension();
        $mimetype = $photo->getMimeType();
        $explode = explode("/",$mimetype);
        $type = $explode[0];

        if (!file_exists($root)) {
            mkdir($root, 0777, true);
        }
        $photo->move($root,$name);
        return $path = "public/resource/".$path."/".$name;
    }

    public function Imagethumbnail($image,$folder,$heigth,$width,$prefix=null)
    {
        $imagename=basename($image);
        $savepath = base_path().'/public/resource/'.$folder.'/';
        if (!file_exists($savepath)) {
            mkdir($savepath, 0777, true);
        }
        //$img = Image::make($image)->resize($heigth, $width)->save($savepath.$prefix.$imagename);
        $img = Image::make($image)->resize(null, 500, function ($constraint) {
            $constraint->aspectRatio();
        })->save($savepath.$prefix.$imagename);
        return 'public/resource/'.$folder.'/'.$imagename;
    }

    public function remove_null($appuser)
    {
        array_walk_recursive($appuser, function (&$item, $key) {
            $item = null === $item ? '' : $item;
        });
    }
}
