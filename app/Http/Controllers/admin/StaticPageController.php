<?php

namespace App\Http\Controllers\admin;

use App\StaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaticPageController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function index()
    {

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if($id==1){
            $data['menu']="About Us";
        }
        if ($id==2){
            $data['menu']="List Your Business";
        }
        $data['id'] = $id;
        $data['static_page'] = StaticPage::findOrFail($id);
        return view('admin.static_page.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $static_page = StaticPage::findOrFail($id);
        $input = $request->all();
        $static_page->update($input);

        if ($id==1) {

            \Session::flash('success', 'About Us has been Updated successfully!');
        }
        if ($id==2){

            \Session::flash('success', 'List Your Business has been Updated successfully!');
        }
        return redirect(config('siteVars.adm_pnl').'/static_page/'.$id.'/edit');
    }

    public function destroy($id)
    {
        //
    }
}
