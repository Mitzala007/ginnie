<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request; 
use App\AppUser; 
use App\Category;
use App\Service;    

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data['mainmenu'] = "";
        $data['appuser'] = AppUser::count();
        $data['category'] = Category::count();
        $data['service'] = Service::count();
        $data['menu'] = "Dashboard";
        return view('admin.dashboard',$data);
    }
}
