<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;
use App\Category;
use Illuminate\Support\Facades\DB;
//use dmin\DB;
class ServiceController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['menu']="Service";
        if ($request->ajax()) {
            $queryString = $request['search'];
                $data['service'] = Service::where(function ($query) use ($queryString) {
                $query->orWhere('name','like','%'.$queryString.'%' );
            })->with(['category'])->orderBy('id','desc')->Paginate($this->pagination);
            return view('admin.service.table',$data);
        }    

        $data['service'] = Service::with(['category'])->orderby('id', 'desc')->paginate($this->pagination);
        return view('admin.service.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $data['menu']="Service";
        $data['category'] = Category::where('status','active')->pluck('name','id')->prepend('Please Select category','');
        return view('admin.service.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' =>'required',
            'status' =>'required'
        ]);
        $input = $request->all();
        Service::create($input);

        \Session::flash('success', 'Serivce has been inserted successfully!');
        return redirect(config('siteVars.adm_pnl').'/service');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu']="Service";
        $data['service'] = Service::findOrFail($id);
        $data['category'] = Category::where('status','active')->pluck('name','id')->prepend('Please Select category','');
        $data['category_selected']= !empty($data['service']['category_id']) ? explode(",",$data['service']['category_id']) : "";
        return view('admin.service.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' =>'required',
            'status' =>'required'
        ]);
        $service = Service::findOrFail($id);
        $input = $request->all();
        $service->update($input);

        \Session::flash('success', 'Serivce has been updated successfully!');
        return redirect(config('siteVars.adm_pnl').'/service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        $service->delete();
        \Session::flash('danger','service has been deleted successfully!');
        return $id;
    }
    public function assign(Request $request)
    {
        $service = Service::findorFail($request['id']);
        $service['status'] = "active";
        $service->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $service = Service::findorFail($request['id']);
        $service['status'] = "inactive";
        $service->update($request->all());
        return $request['id'];
    }
}
