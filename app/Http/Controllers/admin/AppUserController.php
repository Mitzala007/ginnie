<?php

namespace App\Http\Controllers\admin;

use App\AppUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Service;

class AppUserController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data['menu']="AppUser";
        if ($request->ajax()) {
            $queryString = $request['search'];
           $seller = $request['seller'];
           if($request['search'])
           {
                $queryString = str_replace("","%",$queryString);
                $data['app_user'] = AppUser::where(function ($query) use ($queryString) {
                    $query->orWhere('name','like','%'.$queryString.'%' );
                    $query->orWhere('email','like','%'.$queryString.'%');
                })->orderBy('id','desc')->Paginate($this->pagination);
                return view('admin.app_user.table',$data);
            }
            elseif($seller == '1'){
                $data['app_user'] = AppUser::orderby('id', 'desc')->where('service_id')->paginate($this->pagination);
                return view('admin.app_user.table',$data);
            }
            elseif($seller == '2'){
                 $data['app_user'] = AppUser::orderby('id', 'desc')->whereNotNull('service_id')->paginate($this->pagination);
                 return view('admin.app_user.table',$data);
            }else{
                $data['app_user'] = AppUser::orderby('id', 'desc')->paginate($this->pagination);
                return view('admin.app_user.table',$data);
            }
        }
        $data['app_user'] = AppUser::orderby('id', 'desc')->paginate($this->pagination);    
        return view('admin.app_user.index',$data);
    }

    public function create()
    {
        $data['menu']="AppUser";
        $data['service'] = Service::where('status','active')->pluck('name','id'); 
        return view('admin.app_user.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:app_users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|confirmed',
            'phone' => 'required|digits:10',
            'image'=>'mimes:jpeg,bmp,png,jpg',
            'status'=>'required',
        ]);

        $input = $request->all();

        if ($photo = $request->file('image')) {

            $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();

            /*Image Resize Code*/
            $img_resize = Image::make($photo->getRealPath());
            $data = getimagesize($photo);
            $width = $data[0];

            $img_resize->save(public_path('resource/app_users/'.$img_name));
            /*-------------*/
            $input['image'] = "public/resource/app_users/".$img_name;

            /* THUMBNAIL*/

            $path1 =  $this->image($photo,'app_users/thumbnail');
            if($width > 500){
                $input['thumbnail_image'] = $this->Imagethumbnail($path1,'app_users/thumbnail',500,500,null);
            }
            else{
                $input['thumbnail_image'] = $path1;
            }
        }
        if(!empty($request['service_id'])){
            $input['service_id']=implode(',',$request['service_id']);
        }

        AppUser::create($input);

        \Session::flash('success', 'AppUser has been inserted successfully!');
        return redirect(config('siteVars.adm_pnl').'/app_user');
    }

    public function show($id)
    {
        $data['menu'] = "AppUser";
        $data['appuser'] = AppUser::findOrFail($id);
        return view('admin.app_user.show', $data);
    }

    public function edit($id)
    {
        $data['menu']="AppUser";
        $data['app_user'] = AppUser::findOrFail($id);
        $data['service'] = Service::where('status','active')->pluck('name','id');
        $data['service_selected']= !empty($data['app_user']['service_id']) ? explode(",",$data['app_user']['service_id']) : "";
        return view('admin.app_user.edit',$data);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:app_users,email,'.$id.',id,deleted_at,NULL',
            'password' => 'confirmed',
            'phone' => 'required|digits:10',
            'image'=>'mimes:jpeg,bmp,png,jpg',
            'status'=>'required',
        ]);

        if(!empty($request['password'])){
        }
        else{
            unset($request['password']);
        }

        $app_user = AppUser::findOrFail($id);
        $input = $request->all();

        if ($photo = $request->file('image')) {
            if(file_exists($app_user->image))
            {
                unlink($app_user->image);
            }

            if(file_exists($app_user->original_image))
            {
                unlink($app_user->original_image);
            }

            $img_name = str_random(20) . "." . $photo->getClientOriginalExtension();

            /*Image Resize Code*/
            $img_resize = Image::make($photo->getRealPath());
            $data = getimagesize($photo);
            $width = $data[0];

            $img_resize->save(public_path('resource/app_users/'.$img_name));
            /*-------------*/
            $input['image'] = "public/resource/app_users/".$img_name;

            /* THUMBNAIL*/

            $path1 =  $this->image($photo,'app_users/thumbnail');
            if($width > 500){
                $input['thumbnail_image'] = $this->Imagethumbnail($path1,'app_users/thumbnail',500,500,null);
            }
            else{
                $input['thumbnail_image'] = $path1;
            }
        }
        if(!empty($request['service_id'])){
            $input['service_id']=implode(',',$request['service_id']);
        }

        $app_user->update($input);

        \Session::flash('success','AppUser has been updated successfully!');
        return redirect(config('siteVars.adm_pnl').'/app_user');
    }

    public function destroy($id)
    {
        $app_user = AppUser::findOrFail($id);
        $app_user->delete();
        \Session::flash('danger','AppUser has been deleted successfully!');
        return $id;
        //return redirect(config('siteVars.adm_pnl').'/app_user');
    }

    public function assign(Request $request)
    {
        $app_user = AppUser::findorFail($request['id']);
        $app_user['status'] = "active";
        $app_user->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $app_user = AppUser::findorFail($request['id']);
        $app_user['status'] = "inactive";
        $app_user->update($request->all());
        return $request['id'];
    }
}
