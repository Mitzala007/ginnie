<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserRequest;
use App\Service;

class RequestManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['menu']="Request Management";
        $data['service'] = $data['service'] = Service::get();
        if ($request->ajax()) {
            $status = $request['status'];
            $service = $request['service'];
            if(!isset($status) && $service){
                $data['request_management'] = UserRequest::where(function ($query) use ($service) {
                $query->orWhere('service_id','like','%'.$service.'%' );                
                })->with(['user','service'])->orderBy('id','desc')->Paginate($this->pagination);
                return view('admin.request_management.table',$data);
            }
            elseif(isset($status) && empty($service)){
                $data['request_management'] = UserRequest::where(function ($query) use ($status) {
                $query->orWhere('status','like','%'.$status.'%' );                
                })->with(['user','service'])->orderBy('id','desc')->Paginate($this->pagination);
                return view('admin.request_management.table',$data);
            }elseif(isset($status) && isset($service)){            
                $data['request_management'] = UserRequest::where(function ($query) use ($service,$status) {  
                $query->where('status',$status);
                $query->where('service_id',$service); 
                })->with(['user','service',])->orderBy('id','desc')->Paginate($this->pagination);             
                 return view('admin.request_management.table',$data);
            }
            else{
                $data['request_management'] = UserRequest::with(['user','service'])->orderby('id', 'desc')->paginate($this->pagination);
                return view('admin.request_management.table',$data);
            }
        } 
        $data['request_management'] = UserRequest::with(['user','service'])->orderby('id', 'desc')->paginate($this->pagination);
        return view('admin.request_management.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['menu'] = "Request Management";
        $data['request_management'] = UserRequest::findOrFail($id);
        $data['users'] = $data['request_management']->user()->get();
        $data['services'] = $data['request_management']->service()->get();
        //$data['sellers'] = $data['request_management']->seller()->get();      
       
        return view('admin.request_management.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function changeStatus(Request $request)
    {
        $RequestManagement = UserRequest::find($request->id);
        $RequestManagement->status = $request->status;
       $RequestManagement->save();
  
        return response()->json(['success'=>'Status change successfully.']);
    }
}
