<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['menu']="Category";
        if ($request->ajax()) {
            $queryString = $request['search'];
                $data['category'] = Category::where(function ($query) use ($queryString) {
                $query->orWhere('name','like','%'.$queryString.'%' );
            })->orderBy('id','desc')->Paginate($this->pagination);
            return view('admin.category.table',$data);
        }    
    
        $data['category'] = Category::orderby('id', 'desc')->paginate($this->pagination);
        return view('admin.category.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu']="Category";
        return view('admin.category.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' =>'required'
        ]);
        $input = $request->all();
        Category::create($input);

        \Session::flash('success', 'Category has been inserted successfully!');
        return redirect(config('siteVars.adm_pnl').'/category');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu']="Category";
        $data['category'] = Category::findOrFail($id);
        return view('admin.category.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' =>'required'
        ]);
        $Category = Category::findOrFail($id);
        $input = $request->all();
        $Category->update($input);

        \Session::flash('success', 'Category has been updated successfully!');
        return redirect(config('siteVars.adm_pnl').'/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        \Session::flash('danger','Category has been deleted successfully!');
        return $id;
    }
    public function assign(Request $request)
    {
        $category = Category::findorFail($request['id']);
        $category['status'] = "active";
        $category->update($request->all());
        return $request['id'];
    }

    public function unassign(Request $request)
    {
        $category = Category::findorFail($request['id']);
        $category['status'] = "inactive";
        $category->update($request->all());
        return $request['id'];
    }
}
