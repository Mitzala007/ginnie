<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\AppUser;
use App\Service;
//use App\Seller;

class UserRequest extends Model
{
    
    protected $fillable = [
        'user_id','service_id','seller_id','description','status'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at','status',
    ];
    
    public function user()
    {
        return $this->belongsTo(AppUser::class);
    }
    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    // public function seller()
    // {
    //     return $this->belongsTo(Seller::class);
    // }

    const STATUS_NEW = '0';
    const STATUS_INPROGRESS = '1';
    const STATUS_DONE = '2';

    public static $status = [
        self::STATUS_NEW => 'New',
        self::STATUS_INPROGRESS => 'In Progress',
        self::STATUS_DONE => 'Done',
    ];
}
