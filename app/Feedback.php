<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = [
        'user_id','title','description','reply','status'
    ];

    protected $casts = ['user_id' => 'integer' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */



    public function user()
    {
        return $this->belongsTo(AppUser::class);
    }

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    public static $status = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_INACTIVE => 'In Active',
    ];
}
